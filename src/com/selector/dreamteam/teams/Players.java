package com.selector.dreamteam.teams;

import java.util.Arrays;
import java.util.List;

import com.selector.dreamteam.util.DreamTeamConstants;

public class Players {

	private List<String> players = Arrays.asList(DreamTeamConstants.PLAYER1, DreamTeamConstants.PLAYER2 );
	
	public void listPlayers() {
		for(String playerNames:players) {
			System.out.println("Player Name is: "+playerNames);
		}
	}
}
