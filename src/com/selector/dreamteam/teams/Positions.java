package com.selector.dreamteam.teams;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.selector.dreamteam.util.DreamTeamConstants;

public class Positions {

	private List<String> positions = Arrays.asList(DreamTeamConstants.POSTION1, DreamTeamConstants.POSITION2);
	private Map<String, String> playerByPosition = new HashMap<String, String>();

	
	
	
	
	public Positions() {
		super();
		playerByPosition.put(DreamTeamConstants.PLAYER1, DreamTeamConstants.POSTION1);
		playerByPosition.put(DreamTeamConstants.PLAYER2, DreamTeamConstants.POSITION2);
	}

	public void listPositions() {
		for (String position : positions) {
			System.out.println("Position Names are: " + position);
		}
	}
	
	public void findPlayerByPosition(String playerName) {
		System.out.println(playerByPosition.get(playerName));
	}

}
