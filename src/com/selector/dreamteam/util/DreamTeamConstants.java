package com.selector.dreamteam.util;

public class DreamTeamConstants {

	public static final String PLAYER1 = "Matt";
	public static final String PLAYER2 = "Lamar";
	
	public static final String TEAM1 = "Atlanta Falcons";
	public static final String TEAM2 = "Baltimore Ravens";
	
	public static final String POSTION1 = "Quaterback";
	public static final String POSITION2 = "Wide-Reciever";
}
