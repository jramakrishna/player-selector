package com.selector.dreamteam;

import java.util.NoSuchElementException;
import java.util.Scanner;

import com.selector.dreamteam.teams.Players;
import com.selector.dreamteam.teams.Positions;

public class DreamTeam {

	public static void main (String args[]) {
		
        Scanner scanner = new Scanner(System.in);
        try {
            	System.out.println("List of Possible commands:");
            	System.out.println("1 - findPos");
            	System.out.println("2 - listPlayers");
            	System.out.println("3 - possible-commands");
            	System.out.println("Please enter the number for command - ");
                String commandLine = scanner.nextLine();
        		if(commandLine!=null) {
        			if(commandLine.contains("findPos")) {
        				String[] commands = commandLine.split(" ");
        				findPositionOfPlayer(commands[1]);	
        			}else if(commandLine.equals("2")) {
        				listLoadedPlayers();	

        			}else if(commandLine.equals("3")) {
        				System.out.println("These are the possible commands!");
        				System.out.println("findPos");
        				System.out.println("listPlayers");
        			}
        			
        		}
                
        } catch(IllegalStateException | NoSuchElementException e) {
            System.out.println("System.in was closed; exiting");
        } finally {
        	if(scanner!=null) {
        		scanner.close();	
        	}
        	
		}

		

	}
	
	private static void listLoadedPlayers() {
		Players players = new Players();
		players.listPlayers();
	}

	private static void findPositionOfPlayer(String playerName) {
		if(playerName!=null) {
			Positions postions = new Positions();
			postions.findPlayerByPosition(playerName);

		}else {
			System.out.println("Check if player name is sent!");
		}
		
		
	}

	

	
}
